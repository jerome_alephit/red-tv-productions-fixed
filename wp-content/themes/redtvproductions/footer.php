<?php

/**

 * The template for displaying the footer

 *

 * Contains footer content and the closing of the #main and #page div elements.

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */

?>

<div id="footer_section">
<div class="footer_slider">
<div class="info_slider">
<div class="footer_portfolio">
<?php echo do_shortcode('[print_slider_plus_lightbox]'); ?>
</div>
</div>
</div>

<div class="footer_menu">
<div class="footer_logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" ><img src="<?php echo bloginfo('template_url'); ?>/images/footer-logo.png" alt="" /></a></div>
<div class="footer_nav">
<?php wp_nav_menu( array( 'menu' => 'Footer Menu' ) ); ?>
</div>
<div class="clr"></div>
<?php dynamic_sidebar( 'sidebar-9' ); ?>
</div>
</div>

<?php wp_footer(); ?>

</body>
</html>