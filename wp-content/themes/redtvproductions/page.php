<?php

/**

 * The template for displaying all pages

 *

 * This is the template that displays all pages by default.

 * Please note that this is the WordPress construct of pages and that

 * other 'pages' on your WordPress site will use a different template.

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>

<div class="container_section">
<div class="container">
<div class="about_red">
<?php // the_post_thumbnail('medium', array('class' => 'largeimg')); ?>

<?php

	// Start the Loop.

				while ( have_posts() ) : the_post();

					// Include the page content template.

					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.

					//if ( comments_open() || get_comments_number() ) {

					//	comments_template();

					//}

				endwhile;

?>
<div class="clr"></div>
</div>

<?php dynamic_sidebar( 'sidebar-2' ); ?>

<div class="clr"></div>
</div>

<div class="clr"></div>
</div>

<?php get_footer(); ?>

