<?php

/**

 * Template Name: Home Page Template

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>

<div class="contact_bar">
<div class="contact_info">
<?php dynamic_sidebar( 'sidebar-1' ); ?>
<div class="clr"></div>
</div>
</div>
<div class="clr"></div>
<div class="container_section">
<div class="container">

<?php

	// Start the Loop.

				while ( have_posts() ) : the_post();

					// Include the page content template.

					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.

					//if ( comments_open() || get_comments_number() ) {

					//	comments_template();

					//}

				endwhile;

?>	

</div>
<div class="clr"></div>
</div>



<?php get_footer(); ?>

