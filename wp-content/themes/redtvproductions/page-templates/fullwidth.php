<?php

/**

 * Template Name: Full Width Template

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>


<div class="container_section">
<div class="container">

<div class="portfolio_block">
<?php

	// Start the Loop.

				while ( have_posts() ) : the_post();

					// Include the page content template.

					get_template_part( 'content', 'page' );

					// If comments are open or we have at least one comment, load up the comment template.

					//if ( comments_open() || get_comments_number() ) {

					//	comments_template();

					//}

				endwhile;

?>
</div>

<div class="clr"></div>
</div>

<div class="clr"></div>
</div>

<?php get_footer(); ?>