<?php
/**
 * The template for displaying Tag pages
 *
 * Used to display archive-type pages for posts in a tag.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


<?php dynamic_sidebar( 'sidebar-2' ); ?>
            
 <div id="content-area">
<div id="main-content">                       

<h1><?php printf( __( 'Tag Archives: %s', 'twentyfourteen' ), single_tag_title( '', false ) ); ?></h1>

<div class="clr"></div>	
				
<?php query_posts('cat=1&showposts='); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div>
			<h2><a href="<?php the_permalink() ;?>"><?php the_title(); ?></a></h2>
            <small><?php the_time('F jS, Y') ?></small>
			<?php the_excerpt(); ?><a class="blog-read-more" href="<?php the_permalink() ;?>">Read More...</a>
        
        </div>
<hr>
			
<?php endwhile; ?>
<?php endif; ?>


</div>

<div id="sidebar">
<?php dynamic_sidebar( 'sidebar-2_2' ); ?>
<?php dynamic_sidebar( 'sidebar-2_1' ); ?>
</div>


<!-- end sidebar -->
<div class="clr"></div>
	</div>    
	

<?php get_footer(); ?>

