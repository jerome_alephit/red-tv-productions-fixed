$(function() {
	$('#testimonials').jScrollPane({
    scrollbarOnLeft: false,
    dragMinHeight: 28,
    dragMaxHeight: 28,
    scrollbarWidth: 7,
    scrollbarMargin: 47
  });
});