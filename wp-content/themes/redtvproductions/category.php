<?php

/**

 * The template for displaying Category pages

 *

 * @link http://codex.wordpress.org/Template_Hierarchy

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>


<?php dynamic_sidebar( 'sidebar-2' ); ?>
            
 <div id="content-area">
<div id="main-content">                       

<h1><?php printf( __( '%s', 'twentyfourteen' ), single_cat_title( '', false ) ); ?></h1>

<div class="clr"></div>	
				
<?php query_posts('cat=1&showposts='); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div style="border-bottom:1px dashed #ccc; padding-bottom:30px; margin-bottom:14px;" class="post-divider">

                  <div >
                      <h2 style="margin-bottom:0;"><span><a href="<?php the_permalink() ;?>"><?php the_title(); ?></a></span></h2>

                       Published on <meta ><?php the_time('F jS, Y') ?> by <span itemprop="author"><?php the_author(); ?></span>

                       <div itemprop="description">
					   		<div class="feat-img"><a href="<?php the_permalink() ;?>"><?php the_post_thumbnail('thumbnail'); ?></a></div><?php the_excerpt(); ?> <a class="blog-read-more" href="<?php the_permalink() ;?>">Read More...</a>
                       </div>

                       <div style="padding:8px 0px 0px 0px;"> Filed under <span itemprop="keywords"><?php the_tags(); ?> </span></div>
						<div style="clear: both;"></div>
                  </div>

            </div>
			
<?php endwhile; ?>
<?php endif; ?>


</div>

<div id="sidebar">
<?php dynamic_sidebar( 'sidebar-2_2' ); ?>
<?php dynamic_sidebar( 'sidebar-2_1' ); ?>
</div>


<!-- end sidebar -->
<div class="clr"></div>
	</div>    
	

<?php get_footer(); ?>

