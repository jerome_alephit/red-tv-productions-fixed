<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="body_wrapper">           
      <div class="wrapper_container">
          <div id="main">

 <div id="leftsidebar">
<?php
  if($post->post_parent) {
  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
  $titlenamer = get_the_title($post->post_parent);
  }
  else {
  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
  $titlenamer = get_the_title($post->ID);
  }
  if ($children) { ?>
 <?php // echo $titlenamer; ?>
  <ul>
  <?php echo $children; ?>
  </ul>
<?php } ?>
<div class="clear"></div>
</div>


             <?php // dynamic_sidebar( 'sidebar-2' ); ?>
             
               <div id="container">
			   <h2><?php printf( __( 'Search Results for: %s', 'twentyfourteen' ), get_search_query() ); ?></h2>
			   <?php // the_post_thumbnail('medium', array('class' => 'largeimg')); ?>
<?php if ( have_posts() ) : ?>

				<?php
					// Start the Loop.
					while ( have_posts() ) : the_post();

						/*
						 * Include the post format-specific template for the content. If you want to
						 * use this in a child theme, then include a file called called content-___.php
						 * (where ___ is the post format) and that will be used instead.
						 */
						get_template_part( 'content', get_post_format() );

					endwhile;
					// Previous/next post navigation.
					twentyfourteen_paging_nav();

				else :
					// If no content, include the "No posts found" template.
					get_template_part( 'content', 'none' );

				endif;
			?>
			
              </div><!--  #container -->
            <div class="clear"></div>
          </div><!--  #main -->
     </div><!--  #wrapper_container -->
</div>

<?php get_footer(); ?>
