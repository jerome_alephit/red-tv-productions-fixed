<?php

/**

 * The Template for displaying all single posts

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>


<div id="body_wrapper">           
      <div class="wrapper_container">
        <div class="main">
   <div class="inner_heading"><h2><?php the_title(); ?></h2></div>

 <div class="blog_left">
  	 <?php the_post_thumbnail('thumbnail', array('class' => 'thumbs')); ?>
     <div class="clear"></div>
      </div><!-- .container -->
	  
      <div class="blog_right">
	  <h3><?php the_title(); ?><span>- <?php echo get_the_date(); ?></span></h3>
      <?php

				// Start the Loop.

				while ( have_posts() ) : the_post();



					/*

					 * Include the post format-specific template for the content. If you want to

					 * use this in a child theme, then include a file called called content-___.php

					 * (where ___ is the post format) and that will be used instead.

					 */

					get_template_part( 'content', get_post_format() );



					// Previous/next post navigation.

				//	twentyfourteen_post_nav();



					// If comments are open or we have at least one comment, load up the comment template.

					//if ( comments_open() || get_comments_number() ) {

					//	comments_template();

					//}

				endwhile;

			?>
			
       </div>
	 <div class="clear"></div>
	 
	 
   </div><!-- .main -->
   </div><!-- .wrapper_container --> 
</div> 

<?php get_footer(); ?>



