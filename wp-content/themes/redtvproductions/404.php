<?php

/**

 * The template for displaying 404 pages (Not Found)

 *

 * @package WordPress

 * @subpackage Twenty_Fourteen

 * @since Twenty Fourteen 1.0

 */



get_header(); ?>

<div class="container_section">
<div class="container">
<div class="about_red">
<h2 class="main_heading"><?php _e( 'Page Not Found', 'twentyfourteen' ); ?></h2>
<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfourteen' ); ?></p>

<div class="clr"></div>
</div>

<?php dynamic_sidebar( 'sidebar-2' ); ?>

<div class="clr"></div>
</div>

<div class="clr"></div>
</div>


<?php get_footer(); ?>

