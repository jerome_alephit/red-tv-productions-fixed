<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


<?php dynamic_sidebar( 'sidebar-2' ); ?>
            
 <div id="content-area">
<div id="main-content">                       

<h1><?php
						if ( is_day() ) :
							printf( __( 'Daily Archives: %s', 'twentyfourteen' ), get_the_date() );

						elseif ( is_month() ) :
							printf( __( 'Archive for %s', 'twentyfourteen' ), get_the_date( _x( 'F, Y', 'monthly archives date format', 'twentyfourteen' ) ) );

						elseif ( is_year() ) :
							printf( __( 'Yearly Archives: %s', 'twentyfourteen' ), get_the_date( _x( 'Y', 'yearly archives date format', 'twentyfourteen' ) ) );

						else :
							_e( 'Archives', 'twentyfourteen' );

						endif;
					?></h1>

<div class="clr"></div>	
				
<?php query_posts('cat=1&showposts='); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div>
			<h2><a href="<?php the_permalink() ;?>"><?php the_title(); ?></a></h2>
            <small><?php the_time('F jS, Y') ?></small>
			<?php the_excerpt(); ?><a class="blog-read-more" href="<?php the_permalink() ;?>">Read More...</a>
        
        </div>
<hr>
			
<?php endwhile; ?>
<?php endif; ?>


</div>

<div id="sidebar">
<?php dynamic_sidebar( 'sidebar-2_2' ); ?>
<?php dynamic_sidebar( 'sidebar-2_1' ); ?>
</div>


<!-- end sidebar -->
<div class="clr"></div>
	</div>    
	

<?php get_footer(); ?>

